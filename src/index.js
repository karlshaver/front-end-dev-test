// import {ProductBundles} from "./modules/ProductBundles";
import './styles/main.scss';
import * as $ from 'jquery';
import * as moment from 'moment';
import * as tz from 'moment-timezone';
import loadSVGs from './modules/svg-replace';
import 'popper.js';
import 'bootstrap';

document.addEventListener('DOMContentLoaded', () => {
  loadSVGs();

  // Remove call CTA when call center closed
  const removeCall = function removeClosedCallCta() {
    const currentDay = parseInt(moment().tz('America/Los_Angeles').day(), 10);
    const currentHour = parseInt(moment().tz('America/Los_Angeles').format('HHmm'), 10);
    const message = document.getElementById('speak-to-our-bone-specialists');
    const Http = new XMLHttpRequest();
    Http.open('GET', 'https://www.algaecal.com/wp-json/acf/v3/options/options/office_hours');
    Http.send();
    Http.onreadystatechange = () => {
      const hours = JSON.parse(Http.responseText).office_hours[currentDay];
      if (currentHour < hours.starting_time || currentHour > hours.closing_time) {
        message.setAttribute('style', 'display:none!important;');
      }
    };
  };
  removeCall();

  // Play Wistia Video
  window._wq = window._wq || [];
  // Underscores used for wistia api compatability.
  window._wq.push({
    id: '_all',
    onReady(video) {
      const overlay = document.getElementById(`play-button-overlay-${video.hashedId()}`);
      overlay.addEventListener('click', () => {
        overlay.setAttribute('style', 'visibility:hidden;');
        video.play();
      });
    },
  });

  // Remove 0% Discount Bubbles
  const discounts = Object.values(document.getElementsByClassName('percent-off'));
  const checkDiscount = function removeZeroDiscounts(discount) {
    if (discount.dataset.totalSavingsAmount <= 0) {
      discount.setAttribute('style', 'display:none;');
    }
    return discount;
  };
  discounts.map(i => checkDiscount(i));

  // Set equal card heights
  const setHeights = function setElementHeights(className) {
    const heights = [];
    const els = Object.values(document.getElementsByClassName(className));
    els.map((i) => {
      heights.push(i.offsetHeight);
      return i.offsetHeight;
    });
    els.map((i) => {
      i.style.height = `${Math.max(...heights)}px`;
      return i.style.height;
    });
  };
  setTimeout(
    setHeights,
    2000,
    'product-box'
  );

  // Get and place modal content
  $.ajax({
    url: 'https://www.algaecal.com/wp-json/acf/v3/options/options/7yr_full_copy',
    dataType: 'json',
    cache: false,
  }).done((html) => {
    $('.modal-body.guarantee').append(html['7yr_full_copy']);
    return html['7yr_full_copy'];
  });
});
